from ui import(menu,solicitar_valores,solicitar_jugador)
from triqui import(dibujar,jugador,ganador)
tablero=[
['-','-','-'],
['-','-','-'],
['-','-','-'],
]

print('-'*(len(tablero)*4+1))
counter=0
for fila in tablero:
    for elemento in fila:
        print(f'| {elemento} ',end='')
    print('|',counter)
   
    counter=counter+1
    print('-'*(len(tablero)*4+1))
print("  0   1   2")

opcion=0
while opcion != 5:
    print("\n")
    opcion=menu()
    if opcion != 0:
        if opcion ==1:
            print('Donde las quieres ')
            x,y =solicitar_valores()
            solucion=dibujar(x,y)
            numero_jugador=solicitar_jugador()
            emote=jugador(numero_jugador)
            #print(emote)
            tablero[solucion[0]][solucion[1]]=emote
            #print(solucion)

    print("\n")
    print('-'*(len(tablero)*4+1))
    counter=0
    for fila in tablero:
        for elemento in fila:
            print(f'| {elemento} ',end='')
        print('|',counter)
        counter=counter+1
        print('-'*(len(tablero)*4+1))
    print("  0   1   2")
    print("\n")
    ganador(tablero)