#inicializacion 
x=0
while x <5 :
    print(f'x={x}')
    x=x+1

rango_1=range(4)
print(rango_1)
print("="*25)
for numero in range(4,10):
     print(numero, end='---')
print("="*25)
for numero in range(4,21,3):
     print(numero, end='---')

equipos=['Nacional', 'Millonarios' , 'America', 'Junior']
i=0
while i< len(equipos):
    print(equipos[i])
    i=i+1
print("="*25)
for equipo in equipos:
    print(equipo)

tablero=[
['x','o','-','-'],
['o','x','-','-'],
['-','-','-','-'],
['-','-','x','-'],
]
print('-'*(len(tablero)*4+1))
fila=0
while fila < len(tablero):
    col=0
    while col< len(tablero[fila]):
        print(f'|{tablero[fila][col]}', end=' ')
        col=col+1
    print('|')
    print('-'*(len(tablero)*4+1))
    fila=fila+1

for fila in tablero:
    for elemento in fila:
        print(f'| {elemento} ',end='')
    print('|')
    print('-'*(len(tablero)*4+1))