from ui import (
    menu,
    solicitar_valores,
    mostrar_resultado,
    mostrar_opcion_incorrecta,
    mostrar_mensaje_salir)
from funciones import (
    sumar,
    restar,
    multiplicar,
    dividir)

salir=5
SUMAR=1
RESTAR=2
MULTIPLICAR=3
DIVIDIR=4

opcion=0
while opcion != salir:
    opcion = menu()
    x,y =solicitar_valores()
    if opcion==SUMAR:
        resultado= sumar(x,y)
        mostrar_resultado(resultado)
    elif opcion==RESTAR:
        resultado= restar(x,y)
        mostrar_resultado(resultado)
    elif opcion==MULTIPLICAR:
        resultado= multiplicar(x,y)
        mostrar_resultado(resultado)
    elif opcion==DIVIDIR:
        resultado= dividir(x,y)
        mostrar_resultado(resultado)


