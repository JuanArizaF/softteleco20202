def menu():

    '''
    Funcion que muestra el menu y retorma la opcion digitada por el usuariio
    '''
    print('CALCULADORA')
    print('1.Sumar')
    print('2.Restar')
    print('3.Multiplicar')
    print('4.Dividir')
    opcion=input('Digite su opcion.....')
    return int(opcion)

def solicitar_valores():
    '''
    Función que pide 2 enteros al usuario
    '''
    num1 = input('Digite el numero 1... ')
    num2 = input('Digite el numero 2... ')
    return int(num1), int(num2)


def mostrar_resultado(valor):
    '''
    Funcion que muestra el resultado
    '''
    print(f'El resultado es {valor}')


def mostrar_opcion_incorrecta():
    '''
    Funcion que muestra el error
    '''
    print('La opcion digitada es incorrecta!!!')


def mostrar_mensaje_salir():
    '''
    Mensaje de salida
    '''
    print('Gracias por usar nuestros servicios')
