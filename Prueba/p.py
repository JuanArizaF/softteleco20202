alpha = "abcdefghijklmnopqrstuvwxyz"
password = input("What is the password?:")
password = "".join([(str(ord(x)-96) if x.isalpha() else x)
                    for x in list(password)])
password = int(password)


def encrypt(cleartext):
    cyphertext = ""
    for char in cleartext:
        if char in alpha:
            newpos = (alpha.find(char) + password) % 26
            cyphertext += alpha[newpos]
        else:
            cyphertext += char
    
    return cyphertext
    


def decrypt(cleartext):
    cyphertext = ""
    for char in cleartext:
        if char in alpha:
            newpos = (alpha.find(char) - password) % 26
            cyphertext += alpha[newpos]
        else:
            cyphertext += char

    return cyphertext


while True:
    cleartext = input("Cleartext:")
    cleartext = cleartext.lower()
    print(encrypt(cleartext))

    cleartext = input("Cyphertext:")
    cleartext = cleartext.lower()
    print(decrypt(cleartext))
