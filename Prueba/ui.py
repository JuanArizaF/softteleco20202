import getpass
import json
from time import gmtime, localtime, strftime


def menu():
    print('Bienvenido.........')
    print('1.Registrarse......')
    print('2.Iniciar Sesion...')
    print('3.Crear bookmark...')
    opcion=input('Digite su opcion.....')
    return int(opcion)

def crear_bookmark():
    print('Ingrese los siguientes datos \n')
    nombre=input('Nombre del bookmark...')
    url=input('URL...')
    comentario=input('Comentario...')
    labels=input('Labels...')
    login=input('Correo...')
    password = getpass.getpass()
    return(nombre,url,comentario,labels,login,password)

def guardar_bookmark(datos):
    info={
        'nombre': datos[0],
        'url': datos[1],
        'comentario':   datos[2],
        'labels':    datos[3],
        'login':     datos[4],
        'password':     datos[5]
    }
    archivo= open('bookmark.json','a')
    archivo.write('\n')
    json.dump(info,archivo)
    archivo.close


def guardar(datos):
    info={
        'username': datos[0],
        'password': datos[1],
        'nombre':   datos[2],
        'email':    datos[3],
        'hora':     datos[4]
    }
    archivo= open('usuarios.json','a')
    archivo.write('\n')
    json.dump(info,archivo)
    archivo.close

def crear_usuario():
    username=input('Ingrese su Usuario...')
    password = getpass.getpass()
    nombre=input('Ingrese su Nombre...')
    email=input('Ingrese su Email...')
    hora=strftime("%Y-%m-%d- %I:%M:%S", localtime())
    return(username,password,nombre,email,hora)

def iniciar_sesion():
    email=input('Ingrese su Email...')
    password = getpass.getpass()
    archivo= open('usuarios.json', 'r')
    
    for renglon in archivo:
        for renglonsito in renglon:
            print(renglonsito['username'], end='')
    archivo.close
    
   # if email=='juan':
    #    if password=='1234':
    #        print('Hola laura')
    #else:
     #   print("Hola'nt laura")
