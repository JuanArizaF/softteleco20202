cadena = "universidad"
print(cadena[0])
print(cadena[5])

print(cadena[-1])
print(cadena[-2])
print(cadena[0:5])
print(cadena[3:7])
print(cadena[:5])
longitud= len(cadena)
print(longitud)
inicia= cadena.startswith('uni')
print(inicia)

termina= cadena.endswith('idad')
print(termina)

esta= 'si' in cadena
print(esta)

posicion= cadena.find('da')
print(posicion)

nombre= 'Laura'
edad= 20
formato=f'el estudainte {nombre} tiene {edad}'
print(formato)

formato_feo='la estudiante ' + nombre + ' tiene ' + str(edad) + ' años'
print(formato_feo)

frase ='la casa bonita'
print(frase.title())

cantidad=frase.count('a')
print(f'la cantidad es {cantidad}')

resultado= frase.replace('a','4')
print(resultado)

print(frase.upper())

print(frase.lower())

clave= 'abc123'
print(clave.isalnum())
print(clave.isalpha())
print(clave.isdecimal())
print(clave.islower())
print(clave.isupper())