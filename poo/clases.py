class Persona:
    '''
    Clase que maneja los datos de una persona
    '''
    nombre = ''
    edad = 0
    __ciudad = ''   # atributo privado

    def __init__(self, nombre='', edad=0):
        self.nombre = nombre
        self.edad = edad

    def es_adulto(self):
        '''
        Retorna True la persona es adulto y False de lo contrario
        '''
        self.__ciudad = 'Medellin'
        return self.edad >= 18
    
    def __str__(self):
        return f'nombre: {self.nombre}, edad: {self.edad}'


class Empleado(Persona):
    salario = 0

    def __init__(self, salario=0):
        self.salario = salario

    def __str__(self):
        return f'nombre={self.nombre}, salario={self.salario}'


class Curso:
    '''
    Clase que maneja los datos de un curso
    '''
    codigo = 0
    nombre = ''
    estudiantes = []

    def __init__(self, codigo=0, nombre=''):
        '''
        Constructor de Curso
        '''
        self.codigo = codigo
        self.nombre = nombre
        self.estudiantes = []

    def obtener_adultos(self):
        adultos = []
        # Recorre la lista de estudiantes
        for estudiante in self.estudiantes:
            # Verifica si el estudiante es adulto
            if estudiante.es_adulto():
                # Agrega el estudiante a la lisat de adultos
                adultos.append(estudiante)
        return adultos

    def __str__(self):
        cadena = f'codigo={self.codigo}, nombre={self.nombre}\n'
        # Recorre la lista de estudiante
        for estudiante in self.estudiantes:
            cadena += str(estudiante) + '\n'

        return cadena

