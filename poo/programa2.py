from clases import Curso, Persona

p1 = Persona('Hugo', 20)
p2 = Persona('Paco', 21)
p3 = Persona('Luis', 15)
# Creacion del objeto
c1 = Curso(100, 'Programacion')
# Agregar elmentos a un atributo de tipo lista
c1.estudiantes.append(p1)
c1.estudiantes.append(p2)
c1.estudiantes.append(p3)

print(c1)
for e in c1.obtener_adultos():
    print(e)
