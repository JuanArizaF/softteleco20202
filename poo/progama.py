from clases import Curso, Persona, Empleado

print('hellow')

  
#instancia 
p1=Persona()
p1.edad=17
p1.nombre='Juan'
# Lectura atributos
print(p1.nombre)
print(p1.edad)

print(f'{p1.nombre} es adulto ? {p1.es_adulto()}')

print("*"*20)




c1=Curso( 100 ,'Software')
print(c1.codigo)
print(c1.nombre)
c2=Curso(100, 'Programacion')
c2.nombre='base de datos'
c2.codigo=200
print(c2)
c3= Curso(nombre='Circuitos', codigo=101)
print(c3)

c4=Curso(codigo=201)
print(c4)

c5=Curso()
print(c5)
print("*"*20)

e1= Empleado(salario=2000)
e1.nombre="Luis Felioe"
e1.edad=10
print(e1)